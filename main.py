def szyfrowanie(szyfr, tekst):
    sł_szyfr = {}
    sł_szyfr_odwrócony = {}
    for i in szyfr.split("-"):
        sł_szyfr[i[0]] = i[1]
        sł_szyfr_odwrócony[i[1]] = i[0]
    tekst_szyfr = ""
    for i in tekst.upper():
        if(i not in sł_szyfr.keys()):
            if(i not in sł_szyfr_odwrócony.keys()):
                tekst_szyfr += i
            else: tekst_szyfr += sł_szyfr_odwrócony[i]
        else: tekst_szyfr += sł_szyfr[i]
    return tekst_szyfr
def czy_jednoznaczny(szyfr):
    sł_szyfr = {}
    for i in szyfr.split("-"):
        if(i[0] not in sł_szyfr.keys() and i[0] not in sł_szyfr.values()):
            if(i[1] not in sł_szyfr.keys() and i[1] not in sł_szyfr.values()):
                sł_szyfr[i[0]] = i[1]
        else: return False
    return True
def main():
    print("Wybierz rodzaj szyfru: (1 - 'GA-DE-RY-PO-LU-KI', 2 - 'PO-LI-TY-KA-RE-NU', 3 - własny szyfr)")
    szyfr = input()
    if(szyfr == '1'): szyfr = 'GA-DE-RY-PO-LU-KI'
    elif(szyfr == '2'): szyfr = 'PO-LI-TY-KA-RE-NU'
    else:
        print("Podaj własny szyfr: ")
        szyfr = input()
        if(czy_jednoznaczny(szyfr) == False):
            print("Szyfr nieprawidłowy")
            szyfr == ""
    if(szyfr != ""):
        print("Podaj tekst do zaszyfrowania: ")
        tekst = input()
        print(szyfrowanie(szyfr, tekst))
main()
